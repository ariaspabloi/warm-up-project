export const activeArticleMock = {
  _id: '65ae7af21d241e3869ae5a0c',
  author: 'Test Author',
  created_at_i: 123456789,
  external_id: '37229552',
  url: 'testUrl',
  title: 'Test Title',
  isDeleted: false,
  deletedAt: null,
};

export const deletedArticleMock = {
  _id: '65ae7af21d241e3869ae5a1c',
  author: 'Test Author',
  created_at_i: 123456789,
  external_id: '37229553',
  url: 'testUrl',
  title: 'Test Title',
  isDeleted: true,
  deletedAt: 123456789,
};

export const validObjectId = '65ae7af21d241e3869ae5a30';
