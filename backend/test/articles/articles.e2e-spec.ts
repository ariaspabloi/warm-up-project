import { INestApplication, ValidationPipe } from '@nestjs/common';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { ArticlesModule } from '../../src/domain/articles/articles.module';
import { Article } from '../../src/domain/articles/schemas/article.schemas';
import { ArticlesService } from '../../src/domain/articles/service/articles.service';
import {
  activeArticleMock,
  deletedArticleMock,
  validObjectId,
} from './__mocks__/articles.mocks';

describe('[Feature] Articles - /Articles', () => {
  let app: INestApplication;
  let articleModel;
  let articlesService: ArticlesService;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ArticlesModule,
        MongooseModule.forRoot(
          `mongodb://admin:secret@${
            process.env.MONGO_HOST || 'localhost'
          }:27017/test-db?authSource=admin`,
        ),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({ whitelist: true, transform: true }),
    );
    articleModel = moduleFixture.get(getModelToken(Article.name));
    articlesService = app.get<ArticlesService>(ArticlesService);
    jest
      .spyOn(articlesService, 'onModuleInit')
      .mockImplementation(() => Promise.resolve());
    await app.init();
  });

  beforeEach(async () => {
    await articleModel.insertMany([activeArticleMock, deletedArticleMock]);
  });

  afterEach(async () => {
    await articleModel.collection.drop();
  });

  describe('/articles (GET)', () => {
    it('should return all articles that have not been soft deleted', () => {
      return request(app.getHttpServer())
        .get('/articles')
        .expect(200)
        .expect((res) => {
          expect(res.body).toStrictEqual([activeArticleMock]);
        });
    });
  });

  describe('/articles/:id (DELETE)', () => {
    it('should delete article with valid id that is been not soft deleted is provided', () => {
      return request(app.getHttpServer())
        .delete(`/articles/${activeArticleMock._id}`)
        .expect(204);
    });
    it('should return not find error when a valid id of a article that is been soft deleted is provided', () => {
      return request(app.getHttpServer())
        .delete(`/articles/${deletedArticleMock._id}`)
        .expect(404);
    });
    it('should return not find error when an nonexistent id is provided', () => {
      return request(app.getHttpServer())
        .delete(`/articles/${validObjectId}`)
        .expect(404);
    });
    it('should return an error when an invalid id is provided', () => {
      return request(app.getHttpServer()).delete(`/articles/a`).expect(400);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
