import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import helmet from 'helmet';
import { WinstonModule } from 'nest-winston';
import { AppModule } from './app.module';
import { WarmUpConfig } from './config/warm-up-config';
import { loggerOptions } from './utils/logger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger(loggerOptions),
  });
  const configService = app.get(WarmUpConfig);
  const port = configService.appConfiguration.port;
  const globalPrefix = 'api';
  const origin = configService.appConfiguration.corsOrigin;

  app.enableCors({
    origin,
    allowedHeaders: ['Content-Type'],
    methods: ['GET', 'DELETE'],
    preflightContinue: false,
  });
  app.use(helmet());
  app.setGlobalPrefix(globalPrefix);
  app.useGlobalPipes(new ValidationPipe({ transform: true, whitelist: true }));

  await app.listen(port, () => {
    Logger.log(`Server running at http://localhost:${port}/${globalPrefix}`);
  });
}
bootstrap();
