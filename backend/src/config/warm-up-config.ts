import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Environment } from './lib/environment';

@Injectable()
export class WarmUpConfig {
  constructor(private readonly config: ConfigService) {}

  get appConfiguration(): {
    corsOrigin: string;
    port: number;
    mongodbUri: string;
    apiNewsUrl: string;
  } {
    const env = this.config.get('NODE_ENV');
    const apiNewsUrl = this.config.get('API_NEWS_URL');

    const envPrefixes = {
      [Environment.PROD]: 'PROD_',
      [Environment.DEV]: 'DEV_',
    };

    const prefix = envPrefixes[env] || envPrefixes[Environment.DEV];

    const corsOrigin = this.config.get(`${prefix}CORS_ORIGIN`);
    const port = parseInt(this.config.get(`${prefix}PORT`), 10) || 3001;
    const dbName = this.config.get(`${prefix}DB_NAME`);
    const dbHost = this.config.get(`${prefix}DB_HOST`);
    const dbPort = parseInt(this.config.get(`${prefix}DB_PORT`), 10) || 27017;
    const dbUsername = this.config.get(`${prefix}DB_USERNAME`);
    const dbPassword = this.config.get(`${prefix}DB_PASSWORD`);

    const mongodbUri = `mongodb://${dbUsername}:${dbPassword}@${dbHost}:${dbPort}/${dbName}?authSource=admin`;

    return {
      corsOrigin,
      port,
      mongodbUri,
      apiNewsUrl,
    };
  }
}
