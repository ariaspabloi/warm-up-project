import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { WarmUpConfig } from './warm-up-config';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true })],
  providers: [WarmUpConfig],
  exports: [WarmUpConfig],
})
export class WarmUpConfigModule {}
