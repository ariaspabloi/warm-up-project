import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { WarmUpConfig } from './config/warm-up-config';
import { WarmUpConfigModule } from './config/warm-up-config.module';
import { ArticlesModule } from './domain/articles/articles.module';
@Module({
  imports: [
    WarmUpConfigModule,
    MongooseModule.forRootAsync({
      imports: [WarmUpConfigModule],
      useFactory: async (configService: WarmUpConfig) => ({
        uri: configService.appConfiguration.mongodbUri,
      }),
      inject: [WarmUpConfig],
    }),
    ScheduleModule.forRoot(),
    ArticlesModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
