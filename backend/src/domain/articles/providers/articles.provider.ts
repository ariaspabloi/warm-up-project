import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';
import { WarmUpConfig } from '../../../config/warm-up-config';
import { CreateArticleDto } from '../dto/create-article.dto';

@Injectable()
export class ArticlesProvider {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: WarmUpConfig,
  ) {}
  url = this.configService.appConfiguration.apiNewsUrl;

  async fetchArticles(): Promise<CreateArticleDto[]> {
    const firstPageResponse = await firstValueFrom(
      this.httpService.get<{ hits: CreateArticleDto[]; nbPages: number }>(
        `${this.url}&page=0`,
      ),
    );
    const totalPages = firstPageResponse.data.nbPages;

    const allPromises = [];
    for (let page = 1; page < totalPages; page++) {
      allPromises.push(
        firstValueFrom(
          this.httpService.get<{ hits: CreateArticleDto[] }>(
            `${this.url}&page=${page}`,
          ),
        ).then((response) => response.data.hits),
      );
    }
    const pages = await Promise.all(allPromises);

    const allArticles = [...pages.flat(), ...firstPageResponse.data.hits];
    return allArticles;
  }
}
