export const articleProviderFunctionMock = () => {
  return {
    fetchArticles: jest.fn(),
  };
};

export const fetchOlderArticleMock = {
  author: 'Test Author',
  created_at_i: 123456789,
  objectID: '37229549',
  url: 'testUrl',
  title: 'Test Title',
};
export const fetchNewerArticleMock = {
  author: 'Test Author 2',
  created_at_i: 123456790,
  objectID: '37229549',
  url: 'testUrl 2',
  title: 'Test Title 2',
};

export const fetchNullArticleMock = {
  author: null,
  created_at_i: null,
  objectID: null,
  url: null,
  title: null,
};
