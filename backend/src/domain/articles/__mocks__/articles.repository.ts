import { CreateArticleDto } from '../dto/create-article.dto';
import { Article } from '../schemas/article.schemas';

export const articleRepositoryFunctionMock = () => {
  return {
    getAllActiveArticles: jest.fn(),
    upsertArticles: jest.fn(),
    softDeleteOne: jest.fn(),
  };
};

export const insertOlderArticleMock = (() => {
  const newItem = new CreateArticleDto();
  newItem.author = 'Test Author';
  newItem.created_at_i = 123456789;
  newItem.external_id = '37229549';
  newItem.url = 'testUrl';
  newItem.title = 'Test Title';
  return newItem;
})();

export const insertNewerArticleMock = (() => {
  const newItem = new CreateArticleDto();
  newItem.author = 'Test Author 2';
  newItem.created_at_i = 123456790;
  newItem.external_id = '37229549';
  newItem.url = 'testUrl 2';
  newItem.title = 'Test Title 2';
  return newItem;
})();

export const findArticleMock = (() => {
  const newItem = new Article();
  newItem.author = 'Test Author';
  newItem.created_at_i = 123456789;
  newItem.external_id = '37229549';
  newItem.url = 'testUrl';
  newItem.title = 'Test Title';
  newItem.isDeleted = false;
  newItem.deletedAt = null;
  return newItem;
})();

export const removedArticleMock = {
  _id: '65ae7af21d241e3869ae5a1c',
  author: 'Test Author',
  created_at_i: 123456789,
  external_id: '37229549',
  url: 'testUrl',
  title: 'Test Title',
  isDeleted: true,
  deletedAt: new Date(),
};
