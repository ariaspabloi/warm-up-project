import {
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  OnModuleInit,
} from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { PaginationQueryDto } from '../../../common/dto/pagination-query.dto';
import { CreateArticleDto } from '../dto/create-article.dto';
import { ArticlesProvider } from '../providers/articles.provider';
import { ArticlesRepository } from '../repository/articles.repository';

@Injectable()
export class ArticlesService implements OnModuleInit {
  private readonly logger = new Logger(ArticlesService.name);
  constructor(
    private readonly articlesRepository: ArticlesRepository,
    private readonly articlesProvider: ArticlesProvider,
  ) {}

  async findAll(paginationQueryDto: PaginationQueryDto) {
    const articles =
      await this.articlesRepository.getAllActiveArticles(paginationQueryDto);
    return articles;
  }

  async remove(id: string) {
    let result;

    try {
      result = await this.articlesRepository.softDeleteOne(id);
    } catch (error) {
      this.logger.error(
        `Error removing article with id ${id}, error: ${error}`,
      );
      throw new InternalServerErrorException(
        `Error removing article with ${id}`,
      );
    }
    if (!result) throw new NotFoundException(`Article with ${id} not found.`);
    return result;
  }

  @Cron(CronExpression.EVERY_HOUR)
  async updateArticlesDatabase() {
    this.logger.log('Starting articles database update');

    let apiData;
    try {
      apiData = await this.articlesProvider.fetchArticles();
    } catch (error) {
      this.logger.error(
        `Articles database update process failed fetching, error message: ${error?.message}`,
      );
      return;
    }

    let articles = await this.validateArticles(apiData);
    articles = this.reduceValidArticles(articles);
    try {
      await Promise.all(
        articles.map((item) => this.articlesRepository.upsertArticles(item)),
      );
    } catch (error) {
      this.logger.error(
        `Articles database update failed saving, error message: ${error?.message}`,
      );
      return;
    }

    this.logger.log('Articles database update successfully completed');
    return;
  }

  private async validateArticles(articles: CreateArticleDto[]) {
    const validationPromises = articles.map(async (newItem) => {
      const transformedItem = plainToClass(CreateArticleDto, newItem, {
        excludeExtraneousValues: true,
      });
      const errors = await validate(transformedItem);
      return errors.length === 0 ? transformedItem : null;
    });

    const validatedItems = await Promise.all(validationPromises);
    return validatedItems.filter((newItem) => newItem !== null);
  }

  private reduceValidArticles(validatedArticles: CreateArticleDto[]) {
    const groupedItems = validatedArticles.reduce(
      (acc: { [key: string]: CreateArticleDto }, article: CreateArticleDto) => {
        if (
          acc[article.external_id] &&
          acc[article.external_id].created_at_i < article.created_at_i
        ) {
          acc[article.external_id] = {
            ...acc[article.external_id],
            ...article,
          };
        } else if (!acc[article.external_id]) {
          acc[article.external_id] = article;
        }
        return acc;
      },
      {} as { [key: string]: CreateArticleDto },
    );

    const itemsArray = Object.values(groupedItems);
    return itemsArray;
  }

  async onModuleInit(): Promise<void> {
    this.logger.log('Running initial articles database update');
    await this.updateArticlesDatabase();
  }
}
