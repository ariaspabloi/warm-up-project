import {
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { PaginationQueryDto } from '../../../common/dto/pagination-query.dto';
import {
  articleProviderFunctionMock,
  fetchNewerArticleMock,
  fetchNullArticleMock,
  fetchOlderArticleMock,
} from '../__mocks__/articles.provider';
import {
  articleRepositoryFunctionMock,
  findArticleMock,
  insertNewerArticleMock,
  removedArticleMock,
} from '../__mocks__/articles.repository';
import { ArticlesProvider } from '../providers/articles.provider';
import { ArticlesRepository } from '../repository/articles.repository';
import { ArticlesService } from './articles.service';

describe('ArticlesService', () => {
  let service: ArticlesService;
  let articlesRepository: any;
  let articlesProvider: any;

  beforeEach(async () => {
    articlesRepository = articleRepositoryFunctionMock();
    articlesProvider = articleProviderFunctionMock();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticlesService,
        { provide: ArticlesRepository, useValue: articlesRepository },
        { provide: ArticlesProvider, useValue: articlesProvider },
      ],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
    jest
      .spyOn(service, 'onModuleInit')
      .mockImplementation(() => Promise.resolve());
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return all active articles', async () => {
      articlesRepository.getAllActiveArticles.mockResolvedValue([
        findArticleMock,
      ]);

      expect(await service.findAll(new PaginationQueryDto())).toStrictEqual([
        findArticleMock,
      ]);
    });
  });

  describe('remove', () => {
    it('should soft delete a article by id', async () => {
      articlesRepository.softDeleteOne.mockResolvedValue(removedArticleMock);

      expect(await service.remove(removedArticleMock._id)).toBe(
        removedArticleMock,
      );
      expect(articlesRepository.softDeleteOne).toHaveBeenCalledWith(
        removedArticleMock._id,
      );
    });

    it('should throw NotFoundException when the article is not found', async () => {
      articlesRepository.softDeleteOne.mockResolvedValue(undefined);

      await expect(service.remove(removedArticleMock._id)).rejects.toThrow(
        NotFoundException,
      );
      expect(articlesRepository.softDeleteOne).toHaveBeenCalledWith(
        removedArticleMock._id,
      );
    });

    it('should throw InternalServerErrorException when an error occurs', async () => {
      articlesRepository.softDeleteOne.mockImplementation(() => {
        throw new Error();
      });

      await expect(service.remove(removedArticleMock._id)).rejects.toThrow(
        InternalServerErrorException,
      );
      expect(articlesRepository.softDeleteOne).toHaveBeenCalledWith(
        removedArticleMock._id,
      );
    });
  });

  describe('updateArticlesDatabase', () => {
    it('should update the articles database', async () => {
      articlesProvider.fetchArticles.mockResolvedValue([fetchNewerArticleMock]);
      articlesRepository.upsertArticles.mockResolvedValue({});

      await service.updateArticlesDatabase();

      expect(articlesProvider.fetchArticles).toHaveBeenCalled();
      expect(articlesRepository.upsertArticles).toHaveBeenCalledWith(
        insertNewerArticleMock,
      );
    });

    it('should handle error when fetching articles', async () => {
      articlesProvider.fetchArticles.mockImplementation(() => {
        throw new Error();
      });

      await service.updateArticlesDatabase();

      expect(articlesProvider.fetchArticles).toHaveBeenCalled();
      expect(articlesRepository.upsertArticles).not.toHaveBeenCalled();
    });

    it('should handle error when saving articles', async () => {
      articlesProvider.fetchArticles.mockResolvedValue([fetchNewerArticleMock]);
      articlesRepository.upsertArticles.mockImplementation(() => {
        throw new Error();
      });

      await service.updateArticlesDatabase();

      expect(articlesProvider.fetchArticles).toHaveBeenCalled();
      expect(articlesRepository.upsertArticles).toHaveBeenCalledWith(
        insertNewerArticleMock,
      );
    });
  });

  it('should validate articles and filter out invalid ones', async () => {
    articlesProvider.fetchArticles.mockResolvedValue([
      fetchNullArticleMock,
      fetchNewerArticleMock,
    ]);
    articlesRepository.upsertArticles.mockResolvedValue({});

    await service.updateArticlesDatabase();

    expect(articlesProvider.fetchArticles).toHaveBeenCalled();
    expect(articlesRepository.upsertArticles).toHaveBeenCalledWith(
      insertNewerArticleMock,
    );
    expect(articlesRepository.upsertArticles).toHaveBeenCalledTimes(1);
  });

  it('should reduce valid articles to remove duplicates', async () => {
    articlesProvider.fetchArticles.mockResolvedValue([
      fetchOlderArticleMock,
      fetchNewerArticleMock,
    ]);
    articlesRepository.upsertArticles.mockResolvedValue({});

    await service.updateArticlesDatabase();

    expect(articlesProvider.fetchArticles).toHaveBeenCalled();
    expect(articlesRepository.upsertArticles).toHaveBeenCalledWith(
      insertNewerArticleMock,
    );
    expect(articlesRepository.upsertArticles).toHaveBeenCalledTimes(1);
  });
});
