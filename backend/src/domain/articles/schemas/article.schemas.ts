import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ISoftDelete } from '../../../common/plugins/soft-delete/soft-delete.interface';
import { softDeletePlugin } from '../../../common/plugins/soft-delete/soft-delete.plugin';

@Schema({ versionKey: false })
export class Article implements ISoftDelete {
  @Prop({ required: true, unique: true })
  external_id: string;

  @Prop({ required: true })
  author: string;

  @Prop({ required: true })
  url: string;

  @Prop({ required: true })
  title: string;

  @Prop({ required: true, type: Number })
  created_at_i: number;

  @Prop({ type: Boolean, default: false })
  isDeleted: boolean;

  @Prop({ type: Date, default: null })
  deletedAt: Date | null;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
ArticleSchema.plugin(softDeletePlugin);
