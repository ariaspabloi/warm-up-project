import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { WarmUpConfigModule } from '../../config/warm-up-config.module';
import { ArticlesController } from './controller/articles.controller';
import { ArticlesProvider } from './providers/articles.provider';
import { ArticlesRepository } from './repository/articles.repository';
import { Article, ArticleSchema } from './schemas/article.schemas';
import { ArticlesService } from './service/articles.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Article.name, schema: ArticleSchema }]),
    HttpModule,
    WarmUpConfigModule,
  ],
  controllers: [ArticlesController],
  providers: [ArticlesService, ArticlesProvider, ArticlesRepository],
})
export class ArticlesModule {}
