import { IsMongoId } from 'class-validator';

export class ValidateMongoIdDto {
  @IsMongoId()
  id: string;
}
