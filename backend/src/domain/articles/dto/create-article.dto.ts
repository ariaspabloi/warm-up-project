import { Expose, Transform } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';

export class CreateArticleDto {
  @Expose()
  @IsString()
  author: string;

  @Expose()
  @IsNumber()
  created_at_i: number;

  @Expose()
  @Transform(({ obj }) => obj.objectID)
  @IsString()
  external_id: string;

  @Expose()
  @Transform(({ obj }) => obj.story_url || obj.url)
  @IsString()
  url: string;

  @Expose()
  @Transform(({ obj }) => obj.story_title || obj.title)
  @IsString()
  title: string;
}
