import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginationQueryDto } from '../../../common/dto/pagination-query.dto';
import { SoftDeleteModel } from '../../../common/plugins/soft-delete/soft-delete.model';
import { CreateArticleDto } from '../dto/create-article.dto';
import { Article } from '../schemas/article.schemas';

@Injectable()
export class ArticlesRepository {
  constructor(
    @InjectModel(Article.name) private articleModel: SoftDeleteModel<Article>,
  ) {}

  getAllActiveArticles(
    paginationQueryDto: PaginationQueryDto,
  ): Promise<Article[]> {
    const { limit, offset } = paginationQueryDto;
    const articles = this.articleModel
      .findWithSoftDelete()
      .skip(offset)
      .limit(limit)
      .sort({ created_at_i: 'desc' })
      .exec();
    return articles;
  }

  async upsertArticles(createArticleDto: CreateArticleDto): Promise<Article> {
    const options = { upsert: true, new: true, runValidators: true };
    return this.articleModel
      .findOneAndUpdate(
        { external_id: createArticleDto.external_id },
        createArticleDto,
        options,
      )
      .exec();
  }

  softDeleteOne(id: string) {
    return this.articleModel.softDeleteOne({ _id: id, isDeleted: false });
  }
}
