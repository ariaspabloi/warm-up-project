export function softDeletePlugin(schema) {
  schema.add({
    isDeleted: { type: Boolean, default: false },
    deletedAt: { type: Date, default: null },
  });

  schema.statics.findWithSoftDelete = function (filter, options, callback) {
    filter = { ...filter, isDeleted: false };
    return this.find(filter, null, options, callback);
  };

  schema.statics.softDeleteOne = function (filter, options, callback) {
    const update = { isDeleted: true, deletedAt: new Date() };
    return this.findOneAndUpdate(filter, update, options, callback);
  };
}
