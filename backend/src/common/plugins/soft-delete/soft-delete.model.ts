import {
  Callback,
  FilterQuery,
  HydratedDocument,
  Model,
  QueryOptions,
  QueryWithHelpers,
  UpdateWriteOpResult,
} from 'mongoose';

export interface SoftDeleteModel<T> extends Model<T> {
  findWithSoftDelete(
    filter?: FilterQuery<T>,
    options?: QueryOptions | null,
    callback?: Callback,
  ): QueryWithHelpers<Array<HydratedDocument<T>>, HydratedDocument<T>>;

  softDeleteOne(
    filter?: FilterQuery<T>,
    options?: QueryOptions | null,
    callback?: Callback,
  ): QueryWithHelpers<UpdateWriteOpResult, HydratedDocument<T>>;
}
