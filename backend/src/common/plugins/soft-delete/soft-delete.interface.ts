const IS_DELETED_FIELD = 'isDeleted';
const DELETED_AT_FIELD = 'deletedAt';
export interface ISoftDelete {
  [IS_DELETED_FIELD]: boolean;
  [DELETED_AT_FIELD]: Date | null;
}
