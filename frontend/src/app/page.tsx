import { ArticlesList } from "app/components/home/ArticlesList";
import { fetchArticles } from "./action";

export default async function Home() {
  const PAGE_SIZE = 10;
  const data = await fetchArticles(0, PAGE_SIZE);

  if (!data) return <p>error loading articles</p>;
  return (
    <main>
      <ArticlesList initialData={data} PAGE_SIZE={PAGE_SIZE} />
    </main>
  );
}
