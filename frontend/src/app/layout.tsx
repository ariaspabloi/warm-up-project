import { Header } from "app/components/shared/Header";
import "app/sass/globals.sass";
import type { Metadata } from "next";
import { Roboto } from "next/font/google";

const roboto = Roboto({
  weight: ["100", "300", "500", "700"],
  subsets: ["latin-ext"],
});

export const metadata: Metadata = {
  title: "HN Feed",
  description: "Hacker news about NodeJS",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={roboto.className}>
        <Header />
        {children}
      </body>
    </html>
  );
}
