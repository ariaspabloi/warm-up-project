"use server";
import { urls } from "app/api/urls";

export const fetchArticles = async (
  page: number = 0,
  PAGE_SIZE: number,
  skip: number = 0
) => {
  try {
    const res = await fetch(
      `${urls.articles.all}?limit=${PAGE_SIZE}&offset=${
        page * PAGE_SIZE + skip
      }`,
      { cache: "no-store" }
    );
    if (!res.ok) {
      return null;
    }
    const data = await res.json();
    return data;
  } catch (error) {
    console.log(error);
    return null;
  }
};

export const removeArticle = async (_id: string) => {
  try {
    const res = await fetch(urls.articles.delete(_id));
    if (res.status !== 404 && res.status !== 204) {
      return false;
    }
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};
