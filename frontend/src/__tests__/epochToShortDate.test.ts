import epochToShortDate from "../utils/epochToShortDate";

describe("epochToShortDate", () => {
  it("should return the time if the date is within the last 24 hours", () => {
    const now = new Date();
    const twoHoursAgo = new Date(now.getTime() - 2 * 60 * 60 * 1000);
    const expectedTime = `${twoHoursAgo.getHours() % 12 || 12}:${
      twoHoursAgo.getMinutes() < 10
        ? "0" + twoHoursAgo.getMinutes()
        : twoHoursAgo.getMinutes()
    } ${twoHoursAgo.getHours() >= 12 ? "pm" : "am"}`;
    expect(epochToShortDate(twoHoursAgo.getTime())).toBe(expectedTime);
  });

  it('should return "Yesterday" if the date is within the last 48 hours but not today', () => {
    const now = new Date();
    const yesterday = new Date(now.getTime() - 25 * 60 * 60 * 1000);
    expect(epochToShortDate(yesterday.getTime())).toBe("Yesterday");
  });

  it("should return the month and the day if the date is older than 48 hours", () => {
    const oldDate = new Date("2021-01-01T00:00:00");
    expect(epochToShortDate(oldDate.getTime())).toBe("Jan 1");
  });
});
