"use client";
import { fetchArticles, removeArticle } from "app/app/action";
import { Article } from "app/models/Article";
import { useRef, useState } from "react";

export const useArticlesService = (
  initialData: Article[],
  PAGE_SIZE: number
) => {
  const page = useRef(1);
  const deletedCount = useRef(0);
  const lastLength = useRef(initialData.length);
  const [data, setData] = useState<Article[]>(initialData);
  const [error, setError] = useState(false);

  const loadPage = async () => {
    const articles = await fetchArticles(
      page.current,
      PAGE_SIZE,
      deletedCount.current
    );
    if (!articles) {
      setError(true);
      return;
    }
    lastLength.current = articles.length;
    setData((prevData) => [...prevData, ...articles]);
    page.current = page.current + 1;
  };

  const isReachingEnd =
    lastLength.current === 0 || lastLength.current < PAGE_SIZE;

  const handleRemove = async (_id: string) => {
    try {
      const index = data.findIndex((n: Article) => n._id === _id);
      if (index === -1) return;
      const result = await removeArticle(_id);
      if (!result) return;
      setData((prevData) => {
        const updatedArticles = [...prevData];
        updatedArticles.splice(index, 1);
        return updatedArticles;
      });

      deletedCount.current = deletedCount.current + 1;
    } catch (error) {
      console.error("Error removing article:", error);
    }
  };

  return {
    data,
    error,
    handleRemove,
    loadPage,
    isReachingEnd,
  };
};
