import styles from "./Header.module.sass";
export const Header = () => {
  return (
    <header className={styles.Header}>
      <h1>HN Feed</h1>
      <p>We {"<3"} hacker news!</p>
    </header>
  );
};
