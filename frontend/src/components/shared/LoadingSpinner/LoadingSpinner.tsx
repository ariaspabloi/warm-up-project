import { ThreeDots } from "react-loader-spinner";
import styles from "./LoadingSpinner.module.sass";

export const LoadingSpinner = () => {
  return (
    <div className={styles.LoadingSpinner}>
      <ThreeDots
        visible={true}
        height="80"
        width="80"
        color="#4fa94d"
        radius="9"
        ariaLabel="three-dots-loading"
        wrapperStyle={{}}
        wrapperClass=""
      />
    </div>
  );
};
