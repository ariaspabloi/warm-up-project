import { Article } from "app/models/Article";
import epochToShortDate from "app/utils/epochToShortDate";
import { useCallback } from "react";
import { TrashIcon } from "../TrashIcon";
import styles from "./ArticleInfo.module.sass";

export const ArticleInfo = ({
  articleData,
  handleRemove,
}: ArticleInfoProps) => {
  const { _id, title, author, url, created_at_i } = articleData;

  const openUrl = useCallback(() => {
    window.open(url, "_blank");
  }, [url]);

  const onClickRemove = useCallback(
    (event: React.MouseEvent<HTMLElement>) => {
      event.stopPropagation();
      handleRemove(_id);
    },
    [_id, handleRemove]
  );

  return (
    <div className={styles.ArticleInfo} onClick={openUrl}>
      <div>
        <span className={styles.ArticleInfo__title}>{title} </span>
        <span className={styles.ArticleInfo__author}> - {author} -</span>
      </div>
      <span className={styles.ArticleInfo__date}>
        {epochToShortDate(created_at_i)}
      </span>
      <button className={styles.ArticleInfo__icon} onClick={onClickRemove}>
        <TrashIcon width="32px" height="32px" />
      </button>
    </div>
  );
};

interface ArticleInfoProps {
  articleData: Article;
  handleRemove: (_id: string) => void;
}
