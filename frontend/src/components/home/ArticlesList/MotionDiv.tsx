"use client";
import { motion } from "framer-motion";
import { ReactNode } from "react";

const variants = {
  hidden: { opacity: 0 },
  visible: { opacity: 1 },
};

export const MotionDiv = ({
  children,
  index,
}: {
  children: ReactNode;
  index: number;
}) => {
  return (
    <motion.div
      variants={variants}
      initial="hidden"
      animate="visible"
      transition={{
        delay: (index % 20) * 0.1,
        ease: "easeInOut",
        duration: 0.3,
      }}
      viewport={{ amount: 0 }}
      style={{ width: "100%" }}
    >
      {children}
    </motion.div>
  );
};
