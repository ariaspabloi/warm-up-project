"use client";

import { Article } from "app/models/Article";
import { useArticlesService } from "app/hooks/useArticlesService";
import { useMemo } from "react";
import { ArticleInfo } from "../ArticleInfo";
import { InfiniteScroll } from "../InfiniteScroll";
import styles from "./ArticlesList.module.sass";
import { MotionDiv } from "./MotionDiv";

export const ArticlesList = ({ initialData, PAGE_SIZE }: ArticleListProps) => {
  const { data, error, isReachingEnd, loadPage, handleRemove } =
    useArticlesService(initialData, PAGE_SIZE);

  const computedArticles = useMemo(() => {
    return data.map((n: Article, index: number) => (
      <MotionDiv key={n._id} index={index}>
        <ArticleInfo articleData={n} handleRemove={handleRemove} />
      </MotionDiv>
    ));
  }, [data, handleRemove]);

  return (
    <>
      <section className={styles.ArticlesList}>{computedArticles}</section>
      {error && <p>error loading articles</p>}
      {!error && !isReachingEnd && <InfiniteScroll loadPage={loadPage} />}
    </>
  );
};

interface ArticleListProps {
  initialData: Article[];
  PAGE_SIZE: number;
}
