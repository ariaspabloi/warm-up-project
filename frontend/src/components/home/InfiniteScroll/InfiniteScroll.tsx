import { LoadingSpinner } from "app/components/shared/LoadingSpinner";
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";

export const InfiniteScroll = ({ loadPage }: InfiniteScrollProps) => {
  const { ref, inView } = useInView();

  useEffect(() => {
    if (inView) loadPage();
  }, [inView, loadPage]);

  return (
    <div ref={ref}>
      <LoadingSpinner />
    </div>
  );
};

interface InfiniteScrollProps {
  loadPage: () => void;
}
