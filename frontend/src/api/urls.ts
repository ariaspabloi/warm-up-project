const baseURL = `${process.env.BACKEND_URL}/api`;

export const urls = {
  articles: {
    all: `${baseURL}/articles`,
    delete: (_id: string) => `${baseURL}/articles/${_id}`,
  },
};
