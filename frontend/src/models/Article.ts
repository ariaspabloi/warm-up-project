export interface Article {
  _id: string;
  author: string;
  created_at_i: number;
  story_id: string;
  title: string;
  url: string;
}
