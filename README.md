# Warm-up Project

This is a full-stack project that includes a frontend, backend, and MongoDB database. The project is dockerized and can be run in both production and development environments.

![Page image](https://i.postimg.cc/Y0d3tD88/Screenshot-20240130-181619.png)

## Overview

The backend of this project is designed to fetch news from an external API and store it in a database. This data is then exposed to the frontend, where users have the ability to delete stored news items. The application leverages a variety of technologies and practices to provide a robust and scalable solution.

### Technologies Used

- **Node.js & Nest.js**: For building the backend services, offering a scalable framework for server-side applications.
- **React & Next.js**: Utilized on the frontend for dynamic user interfaces and server-side rendering capabilities.
- **TypeScript**: Applied across the project for strong typing, enhancing code quality and maintainability.
- **MongoDB**: The chosen database for storing and managing news data efficiently.
- **Docker**: Containerization of the entire application stack for consistent development, testing, and deployment environments.

### Features

- **Dockerized Environment**: Ensures that the application can be easily set up and run in any environment without the need for complex configurations.
- **GitLab CI Integration**: Automatic testing is performed on pushes, leveraging GitLab CI to maintain code quality and reliability.
- **Unit and E2E Testing**: Comprehensive testing strategy, including unit tests for individual components and end-to-end tests for workflow validation.
- **Soft Delete Implementation**: Articles can be marked as deleted without being permanently removed from the database, allowing for data recovery if necessary.
- **Scheduled Database Updates**: Automated tasks fetch and update the database with the latest news from an external API at scheduled intervals.
- **Server side fetching**
- **Infinite Scroll & Framer Motion**: Enhances user experience on the frontend with smooth infinite scrolling for news items and engaging animations using Framer Motion.

## Running in Production

To run the project in a production environment, execute the `docker-compose.yml` file with the command

```
docker compose up
```

You can change the environment variables for each component as follows:

**Frontend:**
```
- NODE_ENV=production
- BACKEND_URL=http://backend:3001
```

**Backend:**
```
- NODE_ENV=production
- API_NEWS_URL=https://hn.algolia.com/api/v1/search_by_date?query=nodejs
- PROD_PORT=3001
- PROD_CORS_ORIGIN=http://frontend:3000
- PROD_DB_NAME=warm-up-project-db
- PROD_DB_HOST=mongodb
- PROD_DB_PORT=27017
- PROD_DB_USERNAME=admin
- PROD_DB_PASSWORD=secret
```

**MongoDB:**
```
- MONGO_INITDB_ROOT_USERNAME=admin
- MONGO_INITDB_ROOT_PASSWORD=secret
```

## Running in Development

To run the project in a development environment, execute the `docker-compose-dev.yml` file with the command

```
docker compose -f docker-compose-dev.yml up
```

The environment variables are as follows:

**Frontend:**
```
- NODE_ENV=development
- BACKEND_URL=http://backend:3001
```

**Backend:**
```
- NODE_ENV=development
- API_NEWS_URL=https://hn.algolia.com/api/v1/search_by_date?query=nodejs
- DEV_PORT=3001
- DEV_CORS_ORIGIN=*
- DEV_DB_NAME=warm-up-project-db
- DEV_DB_HOST=mongodb
- DEV_DB_PORT=27017
- DEV_DB_USERNAME=admin
- DEV_DB_PASSWORD=secret
```

**MongoDB:**
```
- MONGO_INITDB_ROOT_USERNAME=admin
- MONGO_INITDB_ROOT_PASSWORD=secret
```

## Running Tests

To run the tests:

**Backend:**
Navigate to `/backend` and for unit tests and end-to-end tests, execute

```
npm i
npm run test
npm run test:e2e
```

This will create a temporary MongoDB database using the `backend/docker-compose.yml` file. Note: This is only for local testing. For GitLab CI, tests are run in a container with an existing MongoDB database and executing `npm run test-gitlab:e2e -- articles`.

**Frontend:**
Navigate to `/frontend` and for unit tests execute

```
npm i
npm run test
```

## Running Without Docker

To run the project without Docker:

**Backend:**
Go to `/backend`, rename the file `.env.template` to `.env`, change the values if needed, and for production execute

```
npm i
npm run build & npm run start
```

For development, execute

```
npm i
npm run start:dev
```

**Frontend:**
Go to `/frontend`, rename the file `.env.template` to `.env`, change the values if needed, and for production execute

```
npm i
npm run build & npm run start
```

For development, execute

```
npm i
npm run dev
```

### .env.template Backend example

```
API_NEWS_URL=https://hn.algolia.com/api/v1/search_by_date?query=nodejs

DEV_PORT=3001
DEV_CORS_ORIGIN=*
DEV_DB_NAME=warm-up-project-db
DEV_DB_HOST=mongodb
DEV_DB_PORT=27017
DEV_DB_USERNAME=admin
DEV_DB_PASSWORD=secret

PROD_PORT=3001
PROD_CORS_ORIGIN=http://localhost:3000
PROD_DB_NAME=warm-up-project-db
PROD_DB_HOST=mongodb
PROD_DB_PORT=27017
PROD_DB_USERNAME=admin
PROD_DB_PASSWORD=secret
```

### .env Frontend example

```
BACKEND_URL="http://localhost:
